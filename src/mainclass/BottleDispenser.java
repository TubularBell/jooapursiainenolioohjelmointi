/*
Jooa Pursiainen
0401437
9.11.2016

jiy
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

import java.util.ArrayList;
import java.text.DecimalFormat;

/**
 *
 * @author Jooa
 */
public class BottleDispenser {
    
    
    private ArrayList<Bottle> bottlelist = new ArrayList();
    DecimalFormat df=new DecimalFormat("0.00");
    private float money;
    private int choice;
    static private BottleDispenser bd = null;
    
    private BottleDispenser(){  
        
        money = 0.00f;
        
        Bottle b1 = new Bottle();
        Bottle b2 = new Bottle("Pepsi Max",1.5f,2.20f);
        Bottle b3 = new Bottle("Coca-Cola Zero", 0.5f, 2.00f);
        Bottle b4 = new Bottle("Coca-Cola Zero", 1.5f, 2.50f);
        Bottle b5 = new Bottle("Fanta Zero", 0.5f, 1.95f);
        Bottle b6 = new Bottle("Fanta Zero", 0.5f, 1.95f);
       
   
        bottlelist.add(b1);
        bottlelist.add(b2);
        bottlelist.add(b3);
        bottlelist.add(b4);
        bottlelist.add(b5);
        bottlelist.add(b6);
    }
    static public BottleDispenser getInstance() {
        if (bd == null)
            bd = new BottleDispenser();
        
        return bd;
    }
    
    public float addMoney(double i){
        if (bottlelist.isEmpty() == false){
            money += i;
            System.out.println("Klink! Lisää rahaa laitteeseen!");
            return money;
        }
        else{
            System.out.println("Pullot loppu");
        }
        return money;
    }
    
    public float buyBottle(int i){
        choice = i-1;
        
        if (money >= bottlelist.get(choice).getPrice()){
            
            
            money -=bottlelist.get(choice).getPrice()*1000/1000;
            System.out.println("KACHUNK! " + bottlelist.get(choice).getName()+" tipahti masiinasta!");
            
            bottlelist.remove(choice);
        }
        else {
            System.out.println("Syötä rahaa ensin!");
        }
        return money;
        
    }
    
    public float returnMoney(){
        
        
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df.format(money) + "€" );
        float remoney = money;
        money = (float) 0.00;
        return remoney;
    }
    
    public void printlist(){
        int i;
        for (i=0; i<bottlelist.size();i++){
            System.out.println(i+1 + ". Nimi: " + bottlelist.get(i).getName());
            System.out.println("	Koko: " + bottlelist.get(i).getSize() + "	Hinta: " + bottlelist.get(i).getPrice());
        
        }
        
    }
    public Bottle returnbottle(int i){
        return bottlelist.get(i);
        
        
    }
    public int returnSize(){
        return bottlelist.size();
    }
    
        
    public ArrayList <Bottle> returnlist(){
        return bottlelist;
    }
    
}
