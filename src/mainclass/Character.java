/*

Jooa Pursiainen
0401437
13.10.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

/**
 *
 * @author Jooa
 */
  abstract class Character {
   
    protected String character;
   
    protected String getCharacter(){
        return character;
    }
  }


    class King extends Character{
      public King() {
           character = "King";
           
           
      }
        
     }
    class Knight extends Character{
      public Knight() {
           character = "Knight";
           
           
      }
        
    }

    class Troll extends Character{
          public Troll() {
           character = "Troll";
           
           
      }
        
    }

    class Queen extends Character{
      public Queen() {
           character = "Queen";
           
            
      }
        
    }



 abstract class WeaponBehavior{
   protected String weapon;
   protected String returnWeapon(){
       return(weapon);
   }
    
}
class SwordBehavior extends WeaponBehavior{
    
    public SwordBehavior(){
        weapon = "Sword";
    }
}

class AxeBehavior extends WeaponBehavior{
    
    public AxeBehavior(){
        weapon = "Axe";
    }
}
class ClubBehavior extends WeaponBehavior{
    
    public ClubBehavior(){
        weapon = "Club";
    }
}
class KnifeBehavior extends WeaponBehavior{
    
    public KnifeBehavior(){
        weapon = "Knife";
    }
}