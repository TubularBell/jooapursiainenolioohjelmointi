/*
Jooa Pursiainen
0401437
1.10.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

/**
 *
 * @author Jooa
 */
public class Bottle {
    private String name;
    private float size;
    private float price;
    
    public Bottle(){
        name = "Pepsi Max";
        size =  0.5f;
        price = 1.8f;
    }
    public Bottle(String n,float s, float p){
        name = n;
        size =  s;
        price = p;
        
    }
    
    public String getName(){
        return name;
    }
    
    public float getPrice(){
        return price;
    }
    public float getSize(){
        return size;
    }
    @Override
    public String toString() {
        String restring = name  + " " + Float.toString(size) + " l " + Float.toString(price) + " €";
        return restring;
    }
    
}
