/*

Jooa Pursiainen
0401437
13.10.2016

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

/**
 *
 * @author Jooa
 */
 class Car {
    private String manufacturer;
    private String model;
    
    public Car(){
        Body body = new Body();
        Chassis chassis = new Chassis();
        Engine engine = new Engine();
        Wheel wheel_1 = new Wheel();
        Wheel wheel_2 = new Wheel();
        Wheel wheel_3 = new Wheel();
        Wheel wheel_4 = new Wheel();
        
        System.out.println("Autoon kuuluu:");
        System.out.println("\tBody");
        System.out.println("\tChassis");
        System.out.println("\tEngine");
        System.out.println("\t4 Wheel");
    }
    
   
}
 
 class Body{
     private String colour;
     public Body(){
         System.out.println("Valmistetaan: Body");
         
     }
 }
 
 class Chassis{
     private int rigidy;
     public Chassis(){
         System.out.println("Valmistetaan: Chassis");
     }
 }
 
 
 class Engine{
     private int power;
     public Engine(){
         System.out.println("Valmistetaan: Engine");
     }
     
 }
 class Wheel{
    protected int pressure;
    public Wheel(){
        System.out.println("Valmistetaan: Wheel");
    }
   

}