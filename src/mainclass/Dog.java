/*/*Jooa Pursiainen
0401437
18.9.2016

 */
package mainclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Jooa
 */
public class Dog{
     private String name;
     private String say;
     
     public Dog(){
         name = "Rekku"; 
         say = "Hau!";
         System.out.println("Hei, nimeni on " + name + "!");
     }
     
     public Dog(String n, String s) throws IOException{
         name = n;  
         say = s;
         name = name.trim();
        if (name.isEmpty()){
             name = "Doge";
         }
         System.out.println("Hei, nimeni on " + name);
         
     }
     
   
        
     public void speak() throws IOException {
         say = "";
         while (say.isEmpty()){
             
        
             System.out.print("Mitä koira sanoo: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            say = br.readLine();
            say = say.trim();
             if (say.isEmpty()){
                
                System.out.println(name + ": Much Wow!");
            }
         }
         
         Scanner scan = new Scanner(say);
         while(scan.hasNext()){
             if (scan.hasNextBigInteger()){
                 System.out.println("Such integer: " + scan.next());
                         
             }
             else if (scan.hasNextBoolean()){
                 System.out.println("Such boolean: " + scan.next());
             }
             else {
                 System.out.println(scan.next());
             }
           }
         }
         
        
    }

