/*
Jooa Pursiainen
0401437
18.10.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

import java.util.ArrayList;

/**
 *
 * @author Jooa
 */
public class Bank {
    private String bankAccount;
    private int balance;
    private int deposit;
    private int withdraw;
    private int credit;
    private int i = 0; //Index of accountlist
    
    private ArrayList<Account> accountlist = new ArrayList();
    
    public Bank(){
       //Absolutely nothing here...
    }
    
    public void createCurrentAccount(String s, int b){
        bankAccount = s;
        balance = b;
        
        accountlist.add(new CurrentAccount(bankAccount,balance));
        i = i+1;
    }
    
    public void createCreditAccount(String s, int b, int c){
        bankAccount = s;
        balance = b;
        credit = c;
        
        accountlist.add(new CreditAccount(bankAccount,balance, credit));
        i = i+1;
        
    }
    
    public void deleteAccount(String s){
        bankAccount = s;
        for(int j=0;j < i; j++){
            if(accountlist.get(j).GetAccount().equals(bankAccount)){
                accountlist.remove(j);
                i = i-1;
                System.out.println("Tili poistettu.");
                
            }
            
        }
        
    }
    
    public void addMoney(String s, int d){
        bankAccount = s;
        deposit = d;
        for(int j=0;j < i; j++){
            if(accountlist.get(j).GetAccount().equals(bankAccount)){
                accountlist.get(j).AddMoney(deposit);
               
            }
        }
        
    }
    
    public void withdrawMoney(String s, int w){
        bankAccount = s;
        withdraw = w;
        for(int j=0;j < i; j++){
            if(accountlist.get(j).GetAccount().equals(bankAccount)){
                accountlist.get(j).WithdrawMoney(w);
            }
        }
        
    }
    
    public void printAccount(String s){
        bankAccount = s;
        for(int j=0;j < i; j++){
            if(accountlist.get(j).GetAccount().equals(s)){
                accountlist.get(j).PrintAll();
                
                
            }
            
        }
        //System.out.println("Etsitään tiliä: " + bankAccount);
    }
    
    public void printAll(){
        System.out.println("Kaikki tilit:");
        for(int j=0;j < i; j++){
            accountlist.get(j).PrintAll();
           
        }
    }
}
