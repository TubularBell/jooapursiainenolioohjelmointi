/*
Jooa Pursiainen
0401437
9.10.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Jooa
 */
public class ReadAndWriteIO {
    
    private String Filename;
    private String Inputfile;
    private String Outputfile;
    private String Inputzip;
    
    public ReadAndWriteIO(String s){
        Filename = s;
    }
    
    //Tiedoston luku ruudulle
    public void readFile() throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(Filename));
        String s;
        while ((s = in.readLine()) != null){
           
            System.out.println(s);
            
        }
        in.close();
       
    }
    
    //Tiedoston kirjoituss toiseen tiedostoon
    public void readAndWrite(String x, String y) throws FileNotFoundException, IOException{
        Inputfile = x;
        Outputfile = y;
        
        BufferedReader in = new BufferedReader(new FileReader(Inputfile));
        BufferedWriter out = new BufferedWriter(new FileWriter(Outputfile));
        
        String s;
        while ((s = in.readLine()) != null){
            
            out.write(s);
            out.newLine();
        }
        in.close();
        out.close();
        
    }
    
    //Tiedoston kirjoitus tiedostoon kun rivi on alle 30 merkkiä eikä tyhjä.
     public void readAndWrite_2(String x, String y) throws FileNotFoundException, IOException{
        Inputfile = x;
        Outputfile = y;
        
        BufferedReader in = new BufferedReader(new FileReader(Inputfile));
        BufferedWriter out = new BufferedWriter(new FileWriter(Outputfile));
        
        String s;
        while ((s = in.readLine()) != null){
            if( s.length() < 30 && s.length() > 0 && s.charAt(0) != ' '){
            
                out.write(s);
                out.newLine();
                    
                
            }
        }
        
        in.close();
        out.close();
        
     }
    
     //Tiedoston kirjoitus tiedostoon kun rivi on alle 30 merkkiä, sisältää v-kirjaimen eikä tyhjä.
     public void readAndWrite_3(String x, String y) throws FileNotFoundException, IOException{
        Inputfile = x;
        Outputfile = y;
        
        BufferedReader in = new BufferedReader(new FileReader(Inputfile));
        BufferedWriter out = new BufferedWriter(new FileWriter(Outputfile));
        
        String s;
        while ((s = in.readLine()) != null){
            if( s.length() < 30 && s.length() > 0 && s.charAt(0) != ' ' && s.contains("v") ){
                
                
                out.write(s);
                out.newLine();
                    
                
            }
        }
        
        in.close();
        out.close();
        
     }
     
     // pakatun tiedoston luku
     public void readFileZip(String x) throws FileNotFoundException, IOException{
         Inputzip = x;
         
         ZipFile zf = new ZipFile(Inputzip);
         Enumeration<? extends ZipEntry> entries = zf.entries();
         ZipEntry entry = entries.nextElement();
         InputStream stream = zf.getInputStream(entry);
         
         String s;
         BufferedReader in = new BufferedReader(new InputStreamReader(stream));
         while (((s = in.readLine()) != null)){
            
             
             System.out.println(s);
         }
         
     }
}

    


