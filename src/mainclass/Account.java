/*
Jooa Pursiainen
0401437
18.10.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass;

/**
 *
 * @author Jooa
 */
public abstract class Account {
    protected String accountNumber;
    protected int balance;
    
    
    public String GetAccount(){
        return accountNumber;
    }
    
    public void AddMoney(int m){
        balance = balance + m;
    }
    
    public void WithdrawMoney(int m){
        if(balance > m){
            balance = balance - m;
        }
        else{
            System.out.println("Tilillä ei ole katetta.");
        }
           
        
    }
    public void PrintAll(){
        System.out.println("Tilinumero: " + accountNumber + " Tilillä rahaa: " + balance);
    }
    
        
    
}

  class CurrentAccount extends Account{
    CurrentAccount(String s, int b){
        accountNumber = s;
        balance = b;
        System.out.println("Tili luotu.");
        
    }
    
    
  
}

  class CreditAccount extends Account{
      private int credit;
      CreditAccount(String s, int b, int c){
          accountNumber = s;
          balance = b;
          credit = c;
          System.out.println("Tili luotu.");
      }
      
      @Override
      public void WithdrawMoney(int m){
          if(m < balance + credit){
              balance = balance - m;
              
          }
          else{
              System.out.println("Tilillä ei ole tarpeeksi luottoa.");
          }
      }
    
      @Override
      public void PrintAll(){
        System.out.println("Tilinumero: " + accountNumber + " Tilillä rahaa: " + balance + " Luottoraja: " + credit);
    }
              
  }

