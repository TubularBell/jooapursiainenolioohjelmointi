/*
Jooa Pursiainen
0401437
9.11.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pulloautomaatti;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import mainclass.BottleDispenser;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {
    float moneyf = 0.00f;
    String moneys;
    String receipt = "";
    private ArrayList<String> bottles;
    private ArrayList<String> sizes;
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private Label moneyLabel;
    
    @FXML
    private Button buyButton;
    @FXML
    private TextField messageArea;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Slider moneySlider;
    @FXML
    private ComboBox bottleBox;
    @FXML
    private ComboBox sizeBox;
    @FXML
    private Button receiptButton;
  
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BottleDispenser bd = null;
        bd = BottleDispenser.getInstance(); 
       
        
        
        
        bottles = new ArrayList<String>();
        bottles.add("Pepsi Max");
        bottles.add("Coca-Cola Zero");
        bottles.add("Fanta Zero");
        
        sizes = new ArrayList<String>();
        sizes.add("0.5");
        sizes.add("1.5");
        populateBox();
        
        
    }    

    @FXML
    private void addMoney(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        
        moneyf = bd.addMoney( moneySlider.getValue()*1000/1000);
        moneySlider.setValue(0.00);
        moneys = Float.toString(moneyf);
        moneyLabel.setText(moneys);
        
        
    }

    @FXML
    private void buyBottle(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        int helper = 0;
        for (int i=0; i<bd.returnSize(); i++){
            if(bd.returnbottle(i).getName()== bottleBox.valueProperty().getValue()){
                if(bd.returnbottle(i).getSize()==Float.valueOf(sizeBox.valueProperty().getValue().toString()) ){
                    if(moneyf >= bd.returnbottle(i).getPrice()){
                        messageArea.setText("KACHUNK! " + bd.returnbottle(i).getName()
                            +" tipahti masiinasta!");
                        moneyf = bd.buyBottle(i+1)*1000/1000;
                        receipt = bd.returnbottle(i).getName() + ", " + bd.returnbottle(i).getSize()
                                + "l, " + bd.returnbottle(i).getPrice() + "€";
                        System.out.println(receipt);
                    
                        moneys =Float.toString(moneyf);
                        moneyLabel.setText(moneys);
                    }
                    else{
                        messageArea.setText("Syötä lisää rahaa!");
                    }
                    helper = 1;
                    break;
                }    
            }
        }
        if (helper == 0){
            messageArea.setText("Pulloa ei löydy!");
            
        }
        
        
        
       
       
      
    }

    @FXML
    private void returnMoney(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        float remoney = bd.returnMoney();
        messageArea.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " 
                + remoney + "€");
        moneyf = 0.00f;
        moneys =Float.toString(moneyf);
        moneyLabel.setText(moneys);
        
    }
    private void populateBox(){
        for(String bottle : bottles){
            bottleBox.getItems().add(bottle);
        }
        
        for(String size : sizes){
            sizeBox.getItems().add(size);
        }
    }

    

   

    @FXML
    private void slideMoney(MouseEvent event) {
    }

    @FXML
    private void saveReceipt(ActionEvent event) throws IOException {
        String outputfile = "kuitti.txt";
        try (BufferedWriter BW = new BufferedWriter(new FileWriter(outputfile))) {
            BW.write("Pullokoneen kuitti:");
            BW.newLine();
            BW.write(receipt);
            messageArea.setText("Kuitti kirjoitettu");
        }
        
    }

    

    
    
}
