/*
Jooa Pursiainen
0401437
1.11.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {
    String loadFile;
    
   
    
    
   
    
    
    @FXML
    private TextArea textArea;
    @FXML
    private Button load;
    @FXML
    private Button save;
    @FXML
    private Label inputLabel;
    @FXML
    private Label outputLabel;
    @FXML
    private TextArea inputFile;
    @FXML
    private TextArea outputFile;
    
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         
    }    

    

    @FXML
    private void openFile(ActionEvent event) throws FileNotFoundException, IOException {
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile.getText()))) {
            
            textArea.setText("");
            while ((loadFile = in.readLine()) != null){
                textArea.setText(textArea.getText() + loadFile + "\n");
            }
        }
    }

    @FXML
    private void saveFile(ActionEvent event) throws FileNotFoundException, IOException {
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile.getText()))) {
            if(!"".equals(outputFile.getText())){
                for(String saveFile : textArea.getText().split("\n")){
                    out.write(saveFile);
                    out.newLine();
                
                }
            }
        }

    }
    
}
