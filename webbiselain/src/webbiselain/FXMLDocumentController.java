/*
Jooa Pursiainen
0401437
6.12.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webbiselain;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {
    String address;
    @FXML
    private Button GoButton;
    @FXML
    private TextField UrlField;
    @FXML
    private Button RefreshButton;
    @FXML
    private Button PreviousButton;
    @FXML
    private Button NextButton;
    @FXML
    private WebView web;
    @FXML
    private Button JavaScriptButton;
    @FXML
    private Button InitializeButton;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        eessuntaassun et =null;
        et = eessuntaassun.getInstance();
    }    

    @FXML
    private void GoToPage(ActionEvent event) throws MalformedURLException {
        eessuntaassun et = eessuntaassun.getInstance();
        String s = "http://";
        address = UrlField.getText();
        if("index.html".equals(address)){
            web.getEngine().load((new File(address)).toURI().toURL().toString());
            //web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        }
        else{
            if (address.contains(s)) {
                address = address;
            } else {
                address = s + address;
            }
        web.getEngine().load(address);
        et.makeList(web.getEngine().getLocation());
        
        UrlField.setText(address);
            
        }
       
        
        
    }

    @FXML
    private void RefreshPage(ActionEvent event) {
       web.getEngine().load(web.getEngine().getLocation());
       UrlField.setText(web.getEngine().getLocation());
    }
    

    @FXML
    private void PreviousPage(ActionEvent event) {
        eessuntaassun et = eessuntaassun.getInstance();
        address = et.returnpage(web.getEngine().getLocation());
        web.getEngine().load(address);
        UrlField.setText(address);
    }

    @FXML
    private void NextPage(ActionEvent event) {
        eessuntaassun et = eessuntaassun.getInstance();
        address = et.nextPage(web.getEngine().getLocation());
        web.getEngine().load(address);
        UrlField.setText(address);
    }

    @FXML
    private void JavaScriptAction(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void InitializeButtonAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }

    @FXML
    private void ClickAction(MouseEvent event) {
        eessuntaassun et = eessuntaassun.getInstance();
        et.makeList(web.getEngine().getLocation());
        UrlField.setText(web.getEngine().getLocation());
    }
    
    

    
    
    
}
