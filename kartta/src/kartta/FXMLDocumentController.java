/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private AnchorPane Pohja;
    private ArrayList<Double> cordinates = new ArrayList();
    private ArrayList<Double> circleCordinates = new ArrayList();

    ShapeHandler sh = null;
    private boolean purkka = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void PohjaClickAction(MouseEvent event) {
        sh = ShapeHandler.getInstance();
        if (purkka == false) {
            Point p = new Point();
            Circle c;
            sh = ShapeHandler.getInstance();

            cordinates.add(event.getX());
            cordinates.add(event.getY());
            System.out.println(cordinates.get(0) + "-" + cordinates.get(1));
            c = p.DrawCircle(cordinates);

            c.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                    sh = ShapeHandler.getInstance();
                    purkka = true;
                    System.out.println("Hei olen piste!");
                    circleCordinates.add(c.getLayoutX());
                    circleCordinates.add(c.getLayoutY());

                    if (circleCordinates.size() == 4) {
                        
                        Line line = new Line(circleCordinates.get(0),
                                circleCordinates.get(1), circleCordinates.get(2), circleCordinates.get(3));
                        Pohja.getChildren().add(line);
                        sh.AddLine(line);
                        circleCordinates.clear();

                        
                    }

                }
            });
            Pohja.getChildren().add(c);
            sh.AddCircle(c);

            cordinates.clear();

        }
        purkka = false;
    }


}
