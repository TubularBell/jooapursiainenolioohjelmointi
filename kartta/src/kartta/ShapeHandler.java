/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta;

import java.util.ArrayList;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Jooa
 */
public class ShapeHandler {

    static private ShapeHandler sh = null;
    private ArrayList<Circle> circlelist = new ArrayList();
    private ArrayList<Line> linelist = new ArrayList();
    

    private ShapeHandler() {

    }

    static public ShapeHandler getInstance() {
        if (sh == null) {
            sh = new ShapeHandler();
        }
        return sh;
    }

    public void AddCircle(Circle c) {
        circlelist.add(c);
        

    }
    public void AddLine(Line l){
        linelist.add(l);
    }
    

}
