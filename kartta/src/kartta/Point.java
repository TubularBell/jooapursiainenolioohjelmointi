/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kartta;

import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

/**
 *
 * @author Jooa
 */
public class Point {

    String name;
    double X;
    double Y;
    int radius;

    public Point() {
    }

    public Circle DrawCircle(ArrayList<Double> A) {

        X = A.get(0);
        Y = A.get(1);
        name = X + "-" + Y;
        radius = 5;
        Circle c = new Circle(X);
        c.setLayoutX(X);
        c.setLayoutY(Y);
        c.setRadius(radius);

        return c;

    }

    public String getName() {
        return name;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public int getRadius() {
        return radius;
    }

}
