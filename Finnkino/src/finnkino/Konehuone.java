/*
Jooa Pursiainen
0401437
30.11.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Jooa
 */
public class Konehuone {
    static private Konehuone kh = null;
    private ArrayList<Teatteri> theatrelist = new ArrayList();
    private ArrayList<String> showlist = new ArrayList();
    private String adress;
    private String content;
    private String line;
    private String teatteri;
    private String ID_string;
    private int ID_int;
    private Document doc;
    
    
    private Konehuone() {
        try {
            getXML("http://www.finnkino.fi/xml/TheatreAreas/");
            parsetheater();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(Konehuone.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    private void getXML(String a) throws MalformedURLException, IOException, ParserConfigurationException, SAXException{
        adress = a;
        content = "";
        URL url = new URL(adress);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
   
        while((line = br.readLine()) != null) {
            content = content + line + "\n";
        }
        
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
        doc = dBuilder.parse(new InputSource(new StringReader(content)));
        doc.getDocumentElement().normalize();
        
    }
    
   
    
    private void parsetheater(){
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        //Jätetään pois valitse teatteri ID:t, siksi 1.
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            teatteri = e.getElementsByTagName("Name").item(0).getTextContent();
            ID_string = e.getElementsByTagName("ID").item(0).getTextContent();
            ID_int = Integer.parseInt(ID_string);
            theatrelist.add(new Teatteri( teatteri,ID_int ));
        }
    }
    
    
    public void parseShows(String d, String n, String st, String et) throws IOException, MalformedURLException, ParserConfigurationException, SAXException{
        String date = getDate(d);
        String starttime = getStartTime(st);
        String endtime = getEndTime(et);
        String name = n;
        String address ="";
        int ID = 1029;
        showlist.clear();
        
        //etsitään valittu ID
        for(int i=0; i<theatrelist.size();i++){
            if(theatrelist.get(i).getName() == name){
                ID = theatrelist.get(i).getID();
                break;
            }
        }
        address = "http://www.finnkino.fi/xml/Schedule/?area=" + ID + "&dt=" + date;
        
        getXML(address);
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String show = e.getElementsByTagName("Title").item(0).getTextContent();
            String time = parseTime(e.getElementsByTagName("dttmShowStart").item(0).getTextContent());
            String showdate = parseDate(e.getElementsByTagName("dttmShowStart").item(0).getTextContent());
            if(checkTime(starttime,endtime,time)==1){
                showlist.add(show + "\t" + showdate + "\t" + time);
            }
        }
        
    }
    
    public void parseMovie(String s, String d, String st, String et) throws IOException, MalformedURLException, ParserConfigurationException, SAXException{
        String movie = s;
        String date = getDate(d);
        String starttime = getStartTime(st);
        String endtime = getEndTime(et);
        String address;
        int ID = 1029;
        showlist.clear();
        
        address = "http://www.finnkino.fi/xml/Schedule/?area=" + ID + "&dt=" + date;
        getXML(address);
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String show = e.getElementsByTagName("Title").item(0).getTextContent();
            
            if (show == null ? movie == null : show.equals(movie)){
                
                String theater = e.getElementsByTagName("Theatre").item(0).getTextContent();
                String time = parseTime(e.getElementsByTagName("dttmShowStart").item(0).getTextContent());
                String showdate = parseDate(e.getElementsByTagName("dttmShowStart").item(0).getTextContent());
                if(checkTime(starttime,endtime,time)==1){
                    showlist.add(theater + "\t" + showdate + "\t" + time);
            }
                
            } 
            
        }
        
    }
    public String parseDate(String d){
        String date = d;
        String[] parts = date.split("T");
        String date2 = parts[0];
        return date2;
    }
    public String parseTime(String t){
        String sub = t.substring(11);
        String[] parts = sub.split("\\:");
        String showstart = parts[0] + ":" + parts[1];
        return showstart;
    }
    public int checkTime(String st,String et,String ss){
        
        String[] partsstart = st.split("\\:");
        String starttime = partsstart[0] + partsstart[1];
        int starttimeint = Integer.parseInt(starttime);
        String[] partsend = et.split("\\:");
        String endtime = partsend[0] + partsend[1];
        int endtimeint = Integer.parseInt(endtime);
        String[] showstart = ss.split("\\:");
        String showtime = showstart[0] + showstart[1];
        int showtimeint = Integer.parseInt(showtime);
        
        if(showtimeint>=starttimeint && showtimeint <=endtimeint){
          return 1;
        }
        else{
            return 0;
        }
    }
    
    

    public String getContent() {
        return content;
    }
    public String getDate(String d){
        String date;
         //Päivä
        if(d.matches("\\d{2}.\\d{2}.\\d{4}")){
            date = d;    
        } 
        else{
            Date dToday = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
            date = ft.format(dToday);   
        }
        return date;
    }
    public String getStartTime(String t){
        String starttime;
        //alkuaika
        if(t.matches("\\d{2}:\\d{2}")){
            starttime = t;
        }
        else{
            starttime = "00:00";
        }
        return starttime;
    }
    public String getEndTime(String t){
        String endtime;
        //loppuaika
        if(t.matches("\\d{2}:\\d{2}")){
            endtime = t;
        }
        else{
            endtime = "24:00";
        }
        return endtime;
    }
    
    public ArrayList <Teatteri> returnlist(){
        return theatrelist;
    }
    public ArrayList <String> returnslist(){
        return showlist;
    }
    
    static public Konehuone getInstance() throws IOException, MalformedURLException, ParserConfigurationException, SAXException {
        if (kh == null)
            kh = new Konehuone();
        
        return kh;
    }
}
