/*
Jooa Pursiainen
0401437
28.11.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;

/**
 *
 * @author Jooa
 */
public class Teatteri {
    private String name;
    private int ID;
    
    public Teatteri(String n, int i){
        name = n;
        ID = i;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return ID;
    }
    
    
    
}
