/*
Jooa Pursiainen
0401437
30.11.2016
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Jooa
 */
public class FXMLDocumentController implements Initializable {
    
    private String dateString;
    private String dayFieldString = "";
    @FXML
    private Label label;
    @FXML
    private ComboBox<String> theaterBox;
    @FXML
    private TextField dayField;
    @FXML
    private Button moviesButton;
    @FXML
    private Button nameButton;
    @FXML
    private TextField startField;
    @FXML
    private TextField endField;
    @FXML
    private TextField nameField;
    @FXML
    private ListView<String> movieList;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb)  {
        try {
            Konehuone kh = null;
            kh = Konehuone.getInstance();
            populateTheaterBox();
            
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       
        
    }    

    @FXML
    private void moviesButtonAction(ActionEvent event) throws IOException, MalformedURLException, ParserConfigurationException, SAXException {
        Konehuone kh = Konehuone.getInstance();
        
        kh.parseShows(dayField.getText(), theaterBox.valueProperty().getValue(),startField.getText(),endField.getText());
        
        movieList.getItems().clear();
        for(int i=0;i<kh.returnslist().size();i++){
           movieList.getItems().add(kh.returnslist().get(i));
        }
    }

    @FXML
    private void nameButtonAction(ActionEvent event) throws IOException, MalformedURLException, ParserConfigurationException, SAXException {
         Konehuone kh = Konehuone.getInstance();
         kh.parseMovie(nameField.getText(), dayField.getText(),startField.getText(),endField.getText());
         movieList.getItems().clear();
        for(int i=0;i<kh.returnslist().size();i++){
           movieList.getItems().add(kh.returnslist().get(i));
        }
    }
    
    private void populateTheaterBox() throws IOException, MalformedURLException, ParserConfigurationException, SAXException{
        Konehuone kh = Konehuone.getInstance();

        for(int i =0; i<kh.returnlist().size();i++){
           theaterBox.getItems().add(kh.returnlist().get(i).getName());  
        }   
    }

    
}